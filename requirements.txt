cachetools>=4.2.4
fedora_messaging>=2.0.2
gitpython==3.1.14
gunicorn>=20.0.4
koji>=1.22.1
pyyaml>=5.3.1
requests-kerberos>=0.12.0
